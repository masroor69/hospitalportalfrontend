import React from 'react';
import { Provider } from 'react-redux';
import 'babel-polyfill';
import ReactDOM from 'react-dom';
import App from './Component/AppContiner';
import store from './Component/Store/index';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app'),
);
module.hot.accept();
