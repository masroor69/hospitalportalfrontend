/* eslint-disable max-len */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Zoom from '@material-ui/core/Zoom';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';
import TabContainer from './TabContainer';
import Calender from './Surgerie/Calender';
import SurgeryForm from './Surgerie/SurgeryForm';
import SurgeryAction from './Surgerie/redux/action';
import Manage from './Manage';

const styles = ({
  root: {
    width: '100%',
    height: 'auto'
  },
  tabsContainer: {
    flexGrow: 1,
  },
  fab: {
    position: 'fixed',
    bottom: '5%',
    right: '1%',
  },
});

class MainNavigation extends React.Component {
  state = {
    value: 0,
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    const { classes } = this.props;
    const transitionDuration = {
      enter: 500,
      exit: 500,
    };

    const fabs = [
      {
        color: 'primary',
        className: classes.fab,
        icon: <AddIcon />,
      }
    ];
    return (
      <Fragment >
        <Paper className={classes.tabsContainer}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            fullWidth
            centered
          >
            <Tab label="Schedule" />
            <Tab label="Manage" />
          </Tabs>
        </Paper>
        <SwipeableViews
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}
        >
          <TabContainer >
            <SurgeryForm />
            <Calender />
          </TabContainer>
          <TabContainer >
            <Manage />
          </TabContainer>
        </SwipeableViews>
        {fabs.map((fab, index) => (
          <Zoom
            key={fab.color}
            in={this.state.value === index}
            timeout={transitionDuration}
            style={{
              transitionDelay: this.state.value === index ? transitionDuration.exit : 0,
            }}
            unmountOnExit
          >
            <Tooltip id="tooltip-top" title="Add Surgeries" placement="top">
              <Button variant="fab" className={fab.className} color={fab.color} onClick={this.props.addSurgeryDialogToggle} >
                {fab.icon}
              </Button>
            </Tooltip>
          </Zoom>
        ))}
      </Fragment>
    );
  }
}
MainNavigation.propTypes = {
  classes: PropTypes.object.isRequired,
  addSurgeryDialogToggle: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  addSurgeryExpanded: state.Surgery.addSurgeryExpanded,
});
const mapDispatchToProps = dispatch => bindActionCreators(SurgeryAction, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainNavigation));
