import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import errorActions from './redux/action';

const styles = ({
  close: {
    width: 28,
    height: 28,
  },
});

class Error extends React.Component {
  state = {
    open: true,
  };
  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.props.clearError(this.props.index);
    this.setState({ open: false });
  };

  handleExited = () => {
    this.processQueue();
  };

  render() {
    const { classes, message } = this.props;
    return (
      <div key={new Date().getTime() + Math.round(Math.random() * 1000)}>
        <Snackbar
          key={new Date().getTime() + Math.round(Math.random() * 1000)}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
          onExited={this.handleExited}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
      </div>
    );
  }
}

Error.propTypes = {
  classes: PropTypes.object.isRequired,
  message: PropTypes.string.isRequired,
  clearError: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
};
const mapStateToProps = (state) => ({
  update: state.General.update,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(errorActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Error));
