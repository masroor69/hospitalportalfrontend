import * as types from './Type';

export default function reducer(state = [], action) {
  switch (action.type) {
    case types.ADD_ERROR:
      return [
        ...state,
        action.payload
      ];
    case types.CLEAR_ERROR:
      return state.filter((errorMessage, i) => i !== action.payload);
    default:
      return state;
  }
}
