/* eslint-disable no-unused-vars,import/prefer-default-export */
import * as types from './Type';

export const addError = (errorMassage) => async (dispatch) => {
  dispatch({
    type: types.ADD_ERROR,
    payload: errorMassage,
  });
};

export const clearError = (errorIndex) => async (dispatch) => {
  dispatch({
    type: types.CLEAR_ERROR,
    payload: errorIndex,
  });
};

export default {
  addError,
  clearError,
};
