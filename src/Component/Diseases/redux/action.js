/* eslint-disable no-unused-vars,import/prefer-default-export */
import axios from 'axios';
import * as types from './Type';
import { addError } from '../../Error/redux/action';
import { addingUpdate } from '../../redux/action';

export const addDisDialogToggle = () => async (dispatch) => {
  dispatch({
    type: types.ADD_DISEASE_DIALOG_TOGGLE,
  });
};
export const disArrowBackToggle = () => async (dispatch) => {
  dispatch({
    type: types.DISEASE_ARROW_BACK_TOGGLE,
  });
};
export const addDisease = (disease) => async (dispatch) => {
  dispatch({
    type: types.ADD_DISEASES,
    payload: disease
  });
};

export const fetchingDiseases = () => async (dispatch) => {
  dispatch({
    type: types.FETCH_DISEASES,
  });
  axios.get('https://nodeendpoint.herokuapp.com/disease/diseases')
    .then((response) => {
      dispatch({
        type: types.ADD_DISEASES,
        payload: response.data.disease
      });
    }).catch(error => {
    });
};

export const insertDisease = (data) => async (dispatch) => {
  dispatch({
    type: types.INSERT_DISEASE,
  });
  axios.post('https://nodeendpoint.herokuapp.com/disease/create', data)
    .then((response) => {
      if (response.data.errors) {
        response.data.errors.map((err) =>
          dispatch(addError(err.msg)));
      }
      if (response.data.save) {
        dispatch(addDisDialogToggle());
        dispatch(addError('DISEASE INSERT SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const selectedDisease = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/disease/${id}`)
    .then((response) => {
      dispatch({
        type: types.SELECTED_DISEASES,
        payload: response.data.disease
      });
    }).catch(error => {
      console.log(error.message);
    });
};

export const removeSelectedDisease = () => async (dispatch) => {
  dispatch({
    type: types.REMOVE_SELECTED_DISEASES,
  });
};

export const delteSelectedDisease = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/disease/delete/${id}`)
    .then((response) => {
      if (response.data.delete === 'done') {
        dispatch(disArrowBackToggle());
        dispatch(addError('DISEASE DELETE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const updateSelectedDisease = (id, data) => async (dispatch) => {
  axios.post(`https://nodeendpoint.herokuapp.com/disease/update/${id}`, data)
    .then((response) => {
      if (response.data.update === 'done') {
        dispatch(disArrowBackToggle());
        dispatch(addError('DISEASE UPDATE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};
export default {
  addDisDialogToggle,
  fetchingDiseases,
  addDisease,
  insertDisease,
  selectedDisease,
  removeSelectedDisease,
  disArrowBackToggle,
  delteSelectedDisease,
  updateSelectedDisease,
};
