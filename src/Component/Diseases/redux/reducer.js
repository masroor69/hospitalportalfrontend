import * as types from './Type';

export default function reducer(state = {
  addDiseExpanded: false,
  expanded: false,
}, action) {
  switch (action.type) {
    case types.ADD_DISEASE_DIALOG_TOGGLE:
      return {
        ...state,
        addDiseExpanded: !state.addDiseExpanded
      };
    case types.DISEASE_ARROW_BACK_TOGGLE:
      return {
        ...state,
        expanded: !state.expanded
      };
    case types.ADD_DISEASES:
      return {
        ...state,
        diseases: action.payload
      };
    case types.SELECTED_DISEASES:
      return {
        ...state,
        selectedDiseases: action.payload
      };
    case types.REMOVE_SELECTED_DISEASES:
      return {
        ...state,
        selectedDiseases: {}
      };
    default:
      return state;
  }
}
