/* eslint react/no-unused-state:0,camelcase:0,max-len:0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import diseaseActions from './redux/action';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  }
});

class DiseaseUpdateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      title: '',
      disc: '',
      id: ''
    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      title: nextProps.selectedDiseases.title,
      disc: nextProps.selectedDiseases.disc,
      id: nextProps.selectedDiseases._id,
    });
  }
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    submitForm = () => {
      this.props.updateSelectedDisease(this.state.id, {
        id: this.state.id,
        title: this.state.title,
        disc: this.state.disc,
      });
      this.setState({ isReady: true, debounce: 200 });
    };
  removeDisease = () => {
    this.props.delteSelectedDisease(this.props.selectedDiseases._id);
  };
  render() {
    const { classes } = this.props;
    return (
      <form>
        <div>
          <TextField
            id="Title"
            label="Title"
            placeholder="Physiology etc."
            value={this.state.title}
            onChange={this.handleChange('title')}
            className={classes.textField}
            margin="normal"
          />
          <TextField
            id="disc"
            label="Disc"
            multiline
            placeholder="Discription"
            value={this.state.disc}
            onChange={this.handleChange('disc')}
            className={classes.textField}
            margin="normal"
          />
        </div>
        <Button
          style={styles.button}
          variant="raised"
          color="inherit"
          onClick={this.submitForm}
        >
          UPDATE
        </Button>
        <Button
          style={styles.button}
          variant="raised"
          color="secondary"
          onClick={this.removeDisease}
        >
          DELETE
        </Button>
      </form>
    );
  }
}

DiseaseUpdateForm.propTypes = {
  classes: PropTypes.object.isRequired,
  delteSelectedDisease: PropTypes.func.isRequired,
  updateSelectedDisease: PropTypes.func.isRequired,
  selectedDiseases: PropTypes.object,
};

DiseaseUpdateForm.defaultProps = {
  selectedDiseases: {}
};

const mapStateToProps = (state) => ({
  addExpanded: state.Doctors.addExpanded,
  selectedDiseases: state.Disease.selectedDiseases,
});
const mapDispatchToProps = dispatch => bindActionCreators(diseaseActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DiseaseUpdateForm));

