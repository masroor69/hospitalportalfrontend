/* eslint-disable no-unused-vars,import/prefer-default-export */
import * as types from './Type';

export const addingUpdate = () => async (dispatch) => {
  dispatch({
    type: types.UPDATE_DATA,
  });
};

export default {
  addingUpdate,
};
