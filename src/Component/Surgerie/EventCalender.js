/* eslint no-unused-vars: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import BigCalendar from 'react-big-calendar';
import { withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import style from 'react-big-calendar/lib/css/react-big-calendar.css';
import SurgeryDetailForm from './SurgeryDetailForm';
import surgActions from './redux/action';

const styles = ({
  clander: {
    height: '700px'
  },
});
class EventCalender extends Component {
  getArray = events => {
    const evtArr = [];
    events.forEach((evt) => {
      evtArr.push({
        id: evt._id,
        title: evt.title,
        start: moment(evt.startDate).format('YYYY-MM-DD, h:mm:ss'),
        end: moment(evt.endDate).format('YYYY-MM-DD, h:mm:ss'),
      });
    });
    return evtArr;
  };

  render() {
    BigCalendar.momentLocalizer(moment);
    const { classes } = this.props;
    const events = this.getArray(this.props.events);
    return (
      <div className={classes.clander}>
        <BigCalendar
          selectable
          popup
          view="month"
          startAccessor="start"
          endAccessor="end"
          scrollToTime={new Date(1970, 1, 1, 6)}
          defaultDate={new Date()}
          onSelectEvent={event => this.props.selectedSurgeries(event.id)}
          events={events}
        />
        <SurgeryDetailForm />
      </div>
    );
  }
}


EventCalender.propTypes = {
  classes: PropTypes.object.isRequired,
  events: PropTypes.array.isRequired,
  selectedSurgeries: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({
  addSurgeryExpanded: state.Surgery.addSurgeryExpanded,
});
const mapDispatchToProps = dispatch => bindActionCreators(surgActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(EventCalender));
