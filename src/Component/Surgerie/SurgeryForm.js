/* eslint react/no-unused-state: 0,camelcase: 0,max-len: 0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import moment from 'moment';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import surgActions from './redux/action';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  rootDialog: {
    width: '100%',
  },
  form: {
    minWidth: '70%',
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '40%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  switch: {
    marginLeft: 'auto'
  },
  history: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  historyChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  patientAdd: {
    margin: 10,
    minWidth: 400,
    maxWidth: 700,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 5,
  },
});

class SurgeryForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      open: false,
      active: false,
      title: '',
      room: 'Room 1',
      startTime: '',
      endTime: '',
      Patients: [],
      PatientObject: [],
      doctors: [],
    };
  }
  handleClose = () => {
    this.setState({
      title: '',
      active: 'false',
      startTime: '',
      endTime: '',
      PatientObject: [],
      doctors: [],
      room: 'Room 1',
    });
    this.props.addSurgeryDialogToggle();
  };
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    handleSwitchChange = name => event => {
      this.setState({ [name]: event.target.checked });
    };
    selectHandleChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
    handlePatientChange = event => {
      this.setState({ PatientObject: event.target.value });
    };
    handleDoctorsChange = event => {
      this.setState({ doctors: event.target.value });
    };
    submitForm = () => {
      this.props.insertSurgerie({
        title: this.state.title,
        active: this.state.active,
        startTime: this.state.startTime,
        endTime: this.state.endTime,
        patient: this.state.PatientObject,
        doctors: this.state.doctors,
        room: this.state.room,
      });
    };
    render() {
      const { classes, ...other } = this.props;
      return (
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="simple-dialog-title"
          open={this.props.addSurgeryExpanded}
          className={classes.rootDialog}
          {...other}
        >
          <DialogTitle id="simple-dialog-title">Create Surgerie</DialogTitle>
          <form className={classes.form}>
            <FormGroup row>
              <div>
                <TextField
                  style={{ width: '220%' }}
                  id="title"
                  label="Title"
                  placeholder="Plastic Surgeries etc.."
                  value={this.state.title}
                  onChange={this.handleChange('title')}
                  className={classes.textField}
                  margin="normal"
                />
              </div>
              <div className={classes.switch} >
                <FormControlLabel
                  control={
                    <Switch
                      checked={this.state.active}
                      onChange={this.handleSwitchChange('active')}
                      value="Smoking"
                      color="primary"
                    />
                }
                  label="Active"
                />
              </div>
            </FormGroup>
            <div >
              <FormGroup row>
                <TextField
                  id="StartTime:"
                  label="Start Time:"
                  type="datetime-local"
                  value={this.state.startTime}
                  onChange={this.handleChange('startTime')}
                  className={classes.textField}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  id="EndTime:"
                  label="End Time:"
                  type="datetime-local"
                  value={this.state.endTime}
                  onChange={this.handleChange('endTime')}
                  className={classes.textField}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormGroup>
            </div>
            <div >
              <FormGroup row>
                <div>
                  <React.Fragment>
                    <FormControl className={classes.patientAdd}>
                      <InputLabel htmlFor="select-multiple-chip">Patient</InputLabel>
                      <Select
                        value={this.state.PatientObject}
                        onChange={this.handlePatientChange}
                        input={<Input id="select-multiple-chip" />}
                        renderValue={selected => (
                          <div
                            key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                            className={classes.chips}
                          >
                            {selected.title}. {selected.name}
                          </div>
                        )}
                      >
                        {this.props.patients.map(name => (
                          <MenuItem
                            key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                            value={name}
                          >
                            {name.title}. {name.name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </React.Fragment>
                </div>
                <div>
                  <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="Room">Room</InputLabel>
                    <Select
                      value={this.state.room}
                      onChange={this.selectHandleChange}
                      input={<Input name="room" id="Room" />}
                      autoWidth
                    >
                      <MenuItem value="Room 1">Room 1</MenuItem>
                      <MenuItem value="Room 2">Room 2</MenuItem>
                      <MenuItem value="Room 3">Room 3</MenuItem>
                      <MenuItem value="Room 4">Room 4</MenuItem>
                    </Select>
                    <FormHelperText>Room1,Room2 ....</FormHelperText>
                  </FormControl>
                </div>
              </FormGroup>
              <div>
                <React.Fragment>
                  <FormControl className={classes.patientAdd}>
                    <InputLabel htmlFor="select-multiple-chip">Doctors</InputLabel>
                    <Select
                      multiple
                      value={this.state.doctors}
                      onChange={this.handleDoctorsChange}
                      input={<Input id="select-multiple-chip" />}
                      renderValue={selected => (
                        <div
                          key={new Date().getTime() + Math.round(Math.random() * 1000)}
                          className={classes.chips}
                        >
                          {selected.map(value =>
                            <Chip
                              key={value.id}
                              label={`DR. ${value.title}. ${value.name}`}
                              className={classes.chip}
                            />)}
                        </div>
                        )}
                    >
                      {this.props.doctors.map(name => (
                        <MenuItem
                          key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                          value={name}
                          style={{
                            fontWeight:
                              this.state.doctors.indexOf(name) === -1
                                ? 100
                                : 900,
                          }}
                        >
                          <Checkbox
                            key={new Date().getTime() + Math.round(Math.random() * 1000)}
                            checked={this.state.doctors.indexOf(name) > -1}
                          />
                          {name.title}. {name.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </React.Fragment>
              </div>
            </div>
            <Button
              style={styles.button}
              variant="raised"
              color="primary"
              onClick={this.submitForm}
            >
          Save
            </Button>
          </form>
        </Dialog>
      );
    }
}

SurgeryForm.propTypes = {
  classes: PropTypes.object.isRequired,
  addSurgeryExpanded: PropTypes.bool.isRequired,
  patients: PropTypes.array,
  doctors: PropTypes.array,
  insertSurgerie: PropTypes.func.isRequired,
  addSurgeryDialogToggle: PropTypes.func.isRequired,
};

SurgeryForm.defaultProps = {
  patients: [],
  doctors: [],
};

const mapStateToProps = (state) => ({
  addSurgeryExpanded: state.Surgery.addSurgeryExpanded,
  patients: state.Patient.patients,
  doctors: state.Doctors.doctors,
});
const mapDispatchToProps = dispatch => bindActionCreators(surgActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SurgeryForm));

