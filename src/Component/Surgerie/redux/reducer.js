/* eslint-disable no-unused-vars,import/prefer-default-export */
import * as types from './Type';

export default function reducer(state = {
  addSurgeryExpanded: false,
  detailSurgeryExpanded: false,
  isLoading: true,
  queryString: null,
  expanded: false,
  isFiltered: false,
}, action) {
  switch (action.type) {
    case types.ADD_SURG_DIALOG_TOGGLE:
      return {
        ...state,
        addSurgeryExpanded: !state.addSurgeryExpanded
      };
    case types.DETAIL_SURG_DIALOG_TOGGLE:
      return {
        ...state,
        detailSurgeryExpanded: !state.detailSurgeryExpanded
      };
    case types.IS_LOADING:
      return {
        ...state,
        isLoading: action.payload
      };
    case types.ADD_SURGERIES:
      return {
        ...state,
        surgeries: action.payload
      };
    case types.SELECTED_SURGERIE:
      return {
        ...state,
        selectedSurgerie: action.payload
      };
    case types.REMOVE_SELECTED_SURGERIE:
      return {
        ...state,
        selectedSurgerie: {}
      };
    case types.QUERY_STRING:
      return {
        ...state,
        queryString: action.payload
      };
    case types.IS_FILTERED:
      return {
        ...state,
        isFiltered: action.payload,
      };
    case types.FITERED_SUGS:
      return {
        ...state,
        filtereds: action.payload,
      };
    default:
      return state;
  }
}
