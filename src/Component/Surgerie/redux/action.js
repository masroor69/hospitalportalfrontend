/* eslint-disable no-unused-vars,import/prefer-default-export */
import axios from 'axios';
import * as types from './Type';
import { addError } from '../../Error/redux/action';
import { addingUpdate } from '../../redux/action';

export const addSurgeryDialogToggle = () => async (dispatch) => {
  dispatch({
    type: types.ADD_SURG_DIALOG_TOGGLE,
  });
};
export const detailSurgeryDialogToggle = () => async (dispatch) => {
  dispatch({
    type: types.DETAIL_SURG_DIALOG_TOGGLE,
  });
};
export const addSurgerie = (surgerie) => async (dispatch) => {
  dispatch({
    type: types.ADD_SURGERIES,
    payload: surgerie
  });
};

export const isLoading = (val) => async (dispatch) => {
  dispatch({
    type: types.IS_LOADING,
    payload: val
  });
};

export const fetchingSurgerie = () => async (dispatch) => {
  dispatch({
    type: types.FETCH_SURGERIES,
  });
  axios.get('https://nodeendpoint.herokuapp.com/surgerie/surgeries')
    .then((response) => {
      dispatch(isLoading(false));
      dispatch(addSurgerie(response.data.surgeries));
    }).catch(error => {
      console.log(error.message);
    });
};
export const filterSurgeries = (criteria) => async (dispatch) => {
  dispatch(isLoading(true));
  dispatch({
    type: types.QUERY_STRING,
    payload: null
  });
  axios.post('https://nodeendpoint.herokuapp.com/surgerie/surgeries', criteria)
    .then((response) => {
      dispatch({
        type: types.FITERED_SUGS,
        payload: response.data.surgeries
      });
      dispatch({
        type: types.QUERY_STRING,
        payload: response.data.queryString
      });
      dispatch({
        type: types.IS_FILTERED,
        payload: true
      });
      dispatch(isLoading(false));
      dispatch(addingUpdate());
    }).catch(error => {
      dispatch(isLoading(false));
      console.log(error.message);
    });
};

export const clearFilter = () => async (dispatch) => {
  dispatch(fetchingSurgerie());
  dispatch({
    type: types.QUERY_STRING,
    payload: ''
  });
  dispatch({
    type: types.IS_FILTERED,
    payload: false
  });
  dispatch(isLoading(false));
};

export const insertSurgerie = (data) => async (dispatch) => {
  // TODO solve Async Call in Server
  dispatch({
    type: types.INSERT_SURGERIE,
  });
  axios.post('https://nodeendpoint.herokuapp.com/surgerie/create', data)
    .then((response) => {
      if (response.data.errors) {
        response.data.errors.map((err) =>
          dispatch(addError(err.msg)));
      }
      if (response.data.save) {
        dispatch(addSurgeryDialogToggle());
        dispatch(addError('SURGERIE INSERT SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const selectedSurgeries = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/surgerie/${id}`)
    .then((response) => {
      dispatch({
        type: types.SELECTED_SURGERIE,
        payload: response.data.Surgerie
      });
      dispatch(detailSurgeryDialogToggle());
    }).catch(error => {
      console.log(error.message);
    });
};


export const removeSelectedSurgeries = () => async (dispatch) => {
  dispatch({
    type: types.REMOVE_SELECTED_SURGERIE,
  });
};

export const delteSelectedSurgeries = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/surgerie/delete/${id}`)
    .then((response) => {
      if (response.data.delete === 'done') {
        dispatch(addError('SURGERIE DELETE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export default {
  addSurgeryDialogToggle,
  insertSurgerie,
  detailSurgeryDialogToggle,
  selectedSurgeries,
  removeSelectedSurgeries,
  delteSelectedSurgeries,
  fetchingSurgerie,
  clearFilter,
  filterSurgeries,
};
