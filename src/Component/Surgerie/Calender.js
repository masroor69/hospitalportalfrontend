/* eslint react/no-unused-state: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import EventCalendar from './EventCalender';
import generalActions from './redux/action';
import FilterBox from './FilterBox';

const styles = ({
  button: {
    margin: '8px',
  },
  input: {
    display: 'none',
  },
  loader: {
    margin: '0 auto',
    display: 'block'
  },
});

class Calender extends Component {
  constructor(context) {
    super(context);
    const { surgeries } = this.props;
    this.state = {
      hasToRelod: '',
      items: Object.assign([], surgeries),
    };
  }

  componentWillReceiveProps(nextProps) {
    const { filtereds, surgeries } = nextProps;
    if (nextProps.isFiltered) {
      this.setState({ hasToRelod: true, items: Object.assign([], filtereds) });
    } else {
      this.setState({ items: Object.assign([], surgeries) });
    }
  }
  render() {
    const { items } = this.state;
    return (
      <React.Fragment>
        <FilterBox />
        <br />
        <code>
          { this.props.isLoading &&
            <div><img className="loader" alt="Loading Data" src="./img/Loading.gif" width="50px" /></div>
          }
          { !this.props.isLoading &&
            <EventCalendar events={items} />
          }
        </code>
      </React.Fragment>
    );
  }
}


Calender.propTypes = {
  // update: PropTypes.number.isRequired,
  surgeries: PropTypes.array,
  filtereds: PropTypes.array,
  isLoading: PropTypes.bool.isRequired,
  isFiltered: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  update: state.General.update,
  surgeries: state.Surgery.surgeries,
  isLoading: state.Surgery.isLoading,
  isFiltered: state.Surgery.isFiltered,
  filtereds: state.Surgery.filtereds,
});

Calender.defaultProps = {
  surgeries: [],
  filtereds: [],
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(generalActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Calender));
