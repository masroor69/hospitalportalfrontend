/* eslint max-len: 0, no-nested-ternary: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import surgActions from './redux/action';

const styles = ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: '2rem',
    textAlign: 'left',
  },
  secondaryHeading: {
    fontSize: '1rem',
    textAlign: 'left',
    color: '#b1b1b1',
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    display: 'block',
  },
  column: {
    flexBasis: '33.33%',
  },
  helper: {
    borderLeft: '2px solid gray',
    padding: '10px 20px',
  },
  link: {
    color: 'red',
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  rootDialog: {
    width: '100%',
  },
  form: {
    minWidth: '70%',
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '40%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  switch: {
    marginLeft: 'auto'
  },
  history: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  historyChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  patientAdd: {
    margin: 10,
    minWidth: 400,
    maxWidth: 700,
  },
});
class FilterBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctorObject: '',
      active: ''
    };
  }
  handleSwitchChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };
  handlePatientChange = event => {
    this.setState({ doctorObject: event.target.value });
  };
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };
  clearFilter = () => {
    this.setState({
      doctorObject: '',
      active: '',
      startTime: '',
      endTime: '',
    });
    this.props.clearFilter();
  };
  filterEvents = () => {
    this.setState({
      doctorObject: '',
      active: '',
      startTime: '',
      endTime: '',
    });
    this.props.filterSurgeries({
      doctors: this.state.doctorObject,
      active: this.state.active,
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    });
  };
  render() {
    const { classes } = this.props;
    const label = (this.state.active === '') ? 'Active/ De Active ' : (this.state.active) ? 'active' : 'De Active';
    return (
      <div className={classes.root} >
        <ExpansionPanel >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <div className={classes.column}>
              <Typography className={classes.heading}>Filter Surgery</Typography>
            </div>
            <div className={classes.column}>
              {this.props.queryString && <Typography className={classes.secondaryHeading}> <code>{this.props.queryString }</code></Typography>}
            </div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.details}>
            <div>
              <FormGroup className={classes.formGroup} row>
                <div>
                  <React.Fragment>
                    <FormControl className={classes.patientAdd}>
                      <InputLabel htmlFor="select-multiple-chip">Doctors</InputLabel>
                      <Select
                        value={this.state.doctorObject}
                        onChange={this.handlePatientChange}
                        input={<Input id="select-multiple-chip" />}
                        renderValue={selected => (
                          <div
                            key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                            className={classes.chips}
                          >
                            {selected.title}. {selected.name}
                          </div>
                      )}
                      >
                        {this.props.doctors.map(name => (
                          <MenuItem
                            key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                            value={name}
                          >
                            {name.title} {name.name}
                          </MenuItem>
                      ))}
                      </Select>
                    </FormControl>
                  </React.Fragment>
                </div>
                <div className={classes.switch} >
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.active}
                        onChange={this.handleSwitchChange('active')}
                        value="Smoking"
                        color="primary"
                      />
                  }
                    label={label}
                  />
                </div>
              </FormGroup>
            </div>
            <div>
              <FormGroup row>
                <TextField
                  id="StartTime:"
                  label="Start Time:"
                  type="datetime-local"
                  value={this.state.startTime}
                  onChange={this.handleChange('startTime')}
                  className={classes.textField}
                  margin="normal"
                  InputLabelProps={{
                  shrink: true,
                }}
                />
                <TextField
                  id="EndTime:"
                  label="End Time:"
                  type="datetime-local"
                  value={this.state.endTime}
                  onChange={this.handleChange('endTime')}
                  className={classnames(classes.textField, classes.switch)}
                  margin="normal"
                  InputLabelProps={{
                  shrink: true,
                }}
                />
              </FormGroup>
            </div>
          </ExpansionPanelDetails>
          <Divider />
          <ExpansionPanelActions>
            <Button size="small" onClick={this.clearFilter} >Clear Filter</Button>
            <Button size="small" color="primary" onClick={this.filterEvents}> Filter </Button>
          </ExpansionPanelActions>
        </ExpansionPanel>
      </div>);
  }
}
FilterBox.propTypes = {
  classes: PropTypes.object.isRequired,
  doctors: PropTypes.array,
  queryString: PropTypes.string,
  clearFilter: PropTypes.func.isRequired,
  filterSurgeries: PropTypes.func.isRequired,
};


FilterBox.defaultProps = {
  doctors: [],
  queryString: '',
};

const mapStateToProps = (state) => ({
  doctors: state.Doctors.doctors,
  queryString: state.Surgery.queryString,
});
const mapDispatchToProps = dispatch => bindActionCreators(surgActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(FilterBox));
