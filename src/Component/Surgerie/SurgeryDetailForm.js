/* eslint react/no-unused-state: 0,camelcase: 0,max-len: 0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
// import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import moment from 'moment';
import Select from '@material-ui/core/Select';
import surgActions from './redux/action';

const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  rootDialog: {
    width: '100%',
  },
  form: {
    minWidth: '70%',
  },
  button: {
    width: '90%',
    margin: '0 auto',
    marginBottom: '10px',
    display: 'block',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '40%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  switch: {
    marginLeft: 'auto'
  },
  history: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  historyChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  patientAdd: {
    margin: 10,
    minWidth: 400,
    maxWidth: 700,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 5,
  },
});

class SurgeryDetailForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      open: false,
      active: false,
      title: '',
      room: 'Room 1',
      startTime: '',
      endTime: '',
      Patients: [],
      PatientObject: [],
      doctors: [],
    };
  }
  handleDelete = () => {
    const selectedId = this.props.selectedSurgerie._id;
    this.handleClose();
    this.props.delteSelectedSurgeries(selectedId);
  };
  handleClose = () => {
    this.props.detailSurgeryDialogToggle();
    this.props.removeSelectedSurgeries();
  };
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    handleSwitchChange = name => event => {
      this.setState({ [name]: event.target.checked });
    };
    selectHandleChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
    render() {
      const { classes, ...other } = this.props;
      console.log(this.props.selectedSurgerie);
      return (
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="simple-dialog-title"
          open={this.props.detailSurgeryExpanded}
          className={classes.rootDialog}
          {...other}
        >
          <DialogTitle id="simple-dialog-title">Surgerie Detail</DialogTitle>
          <form className={classes.form}>
            <FormGroup row>
              <div>
                <TextField
                  disabled
                  style={{ width: '220%' }}
                  id="title"
                  label="Title"
                  placeholder="Plastic Surgeries etc.."
                  value={this.props.selectedSurgerie.title}
                  onChange={this.handleChange('title')}
                  className={classes.textField}
                  margin="normal"
                />
              </div>
              <div className={classes.switch} >
                <FormControlLabel
                  disabled
                  control={
                    <Switch
                      checked={this.props.selectedSurgerie.active}
                      onChange={this.handleSwitchChange('active')}
                      value="Smoking"
                      color="primary"
                    />
                }
                  label="Active"
                />
              </div>
            </FormGroup>
            <div >
              <FormGroup row>
                <TextField
                  disabled
                  id="StartTime:"
                  label="Start Time:"
                  value={moment(this.props.selectedSurgerie.startDate).utc().format('YYYY-MM-DD HH:mm:ss')}
                  onChange={this.handleChange('startTime')}
                  className={classes.textField}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  disabled
                  id="EndTime:"
                  label="End Time:"
                  value={moment(this.props.selectedSurgerie.endDate).utc().format('YYYY-MM-DD HH:mm:ss')}
                  onChange={this.handleChange('endTime')}
                  className={classes.textField}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormGroup>
            </div>
            <div >
              <FormGroup row>
                <div>
                  <React.Fragment>
                    Patient:
                    <FormControl className={classes.patientAdd}>
                      {this.props.selectedSurgerie && this.props.selectedSurgerie.patient &&
                        <Chip
                          key={this.props.selectedSurgerie.patient._id}
                          label={`${this.props.selectedSurgerie.patient.title}. ${this.props.selectedSurgerie.patient.name}`}
                          className={classes.chip}
                        />
                      }
                    </FormControl>
                  </React.Fragment>
                </div>
                <div>
                  <FormControl className={classes.formControl} disabled>
                    <InputLabel htmlFor="Room">Room</InputLabel>
                    <Select
                      value={this.props.selectedSurgerie.room}
                      onChange={this.selectHandleChange}
                      input={<Input name="room" id="Room" />}
                      autoWidth
                    >
                      <MenuItem value="Room 1">Room 1</MenuItem>
                      <MenuItem value="Room 2">Room 2</MenuItem>
                      <MenuItem value="Room 3">Room 3</MenuItem>
                      <MenuItem value="Room 4">Room 4</MenuItem>
                    </Select>
                    <FormHelperText>Room1,Room2 ....</FormHelperText>
                  </FormControl>
                </div>
              </FormGroup>
              <div>
                <React.Fragment>
                  Doctors:
                  <FormControl className={classes.patientAdd}>
                    {this.props.selectedSurgerie && this.props.selectedSurgerie.doctor &&
                    this.props.selectedSurgerie.doctor.map((doc) => (<Chip
                      key={doc._id}
                      label={`DR. ${doc.title}. ${doc.name}`}
                      className={classes.chip}
                    />))
                    }
                  </FormControl>
                </React.Fragment>
              </div>
            </div>
            <Button
              style={styles.button}
              variant="raised"
              color="secondary"
              onClick={this.handleDelete}
            >
              Delete
            </Button>
          </form>
        </Dialog>
      );
    }
}

SurgeryDetailForm.propTypes = {
  classes: PropTypes.object.isRequired,
  detailSurgeryExpanded: PropTypes.bool.isRequired,
  detailSurgeryDialogToggle: PropTypes.func.isRequired,
  removeSelectedSurgeries: PropTypes.func.isRequired,
  delteSelectedSurgeries: PropTypes.func.isRequired,
  selectedSurgerie: PropTypes.object,
};
SurgeryDetailForm.defaultProps = {
  selectedSurgerie: {}
};

const mapStateToProps = (state) => ({
  detailSurgeryExpanded: state.Surgery.detailSurgeryExpanded,
  selectedSurgerie: state.Surgery.selectedSurgerie,
});
const mapDispatchToProps = dispatch => bindActionCreators(surgActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SurgeryDetailForm));

