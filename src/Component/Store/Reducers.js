import { combineReducers } from 'redux';
import GeneralReducers from '../redux/reducer';
import DoctorsReducers from '../Doctors/redux/reducer';
import ErrorsReducers from '../Error/redux/reducer';
import ProficiencyReducers from '../Proficiencys/redux/reducer';
import DiseaseReducers from '../Diseases/redux/reducer';
import PatientReducers from '../Patients/redux/reducer';
import SurgeryReducers from '../Surgerie/redux/reducer';

export default combineReducers({
  General: GeneralReducers,
  Errors: ErrorsReducers,
  Doctors: DoctorsReducers,
  Proficiency: ProficiencyReducers,
  Disease: DiseaseReducers,
  Patient: PatientReducers,
  Surgery: SurgeryReducers,
});
