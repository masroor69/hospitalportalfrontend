import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './Reducers';
import { fetchingDoctors } from '../Doctors/redux/action';
import { fetchingProficiency } from '../Proficiencys/redux/action';
import { fetchingDiseases } from '../Diseases/redux/action';
import { fetchingPatients } from '../Patients/redux/action';
import { fetchingSurgerie } from '../Surgerie/redux/action';

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk),
);
store.dispatch(fetchingDoctors());
store.dispatch(fetchingProficiency());
store.dispatch(fetchingDiseases());
store.dispatch(fetchingPatients());
store.dispatch(fetchingSurgerie());
export default store;
