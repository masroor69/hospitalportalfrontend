/* eslint react/no-unused-state: 0  camelcase: 0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import patientActions from './redux/action';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  history: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  historyChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  historyAdd: {
    margin: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 10,
  },
  switch: {
    marginLeft: 'auto'
  }
});

class PatientForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      smoking: '',
      title: '',
      name: '',
      age: '',
    };
  }
  selectHandleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSwitchChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };
    submitForm = () => {
      this.props.insertPatient({
        smoking: this.state.smoking,
        title: this.state.title,
        name: this.state.name,
        age: this.state.age,
      });
      this.setState({ isReady: true, debounce: 200 });
    };
    render() {
      const { classes } = this.props;
      return (
        <form>
          <div>
            <FormGroup row>
              <div>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="title">Title</InputLabel>
                  <Select
                    value={this.state.title}
                    onChange={this.selectHandleChange}
                    input={<Input name="title" id="title" />}
                    autoWidth
                  >
                    <MenuItem value="Mr">Mr</MenuItem>
                    <MenuItem value="Mrs">Mrs</MenuItem>
                    <MenuItem value="Miss">Miss</MenuItem>
                  </Select>
                  <FormHelperText>Mr,Mrs,Miss</FormHelperText>
                </FormControl>
              </div>
              <div className={classes.switch} >
                <FormControlLabel
                  control={
                    <Switch
                      checked={this.state.smoking}
                      onChange={this.handleSwitchChange('smoking')}
                      value="Smoking"
                      color="primary"
                    />
                  }
                  label="Smoking"
                />
              </div>
            </FormGroup>
            <div >
              <TextField
                id="FullName"
                label="Full Name"
                placeholder="John Doe"
                value={this.state.name}
                onChange={this.handleChange('name')}
                className={classes.textField}
                margin="normal"
              />
            </div>
            <div>
              <TextField
                id="age"
                label="age"
                value={this.state.age}
                onChange={this.handleChange('age')}
                type="number"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
              />
            </div>
          </div>
          <Button
            style={styles.button}
            variant="raised"
            color="primary"
            onClick={this.submitForm}
          >
          Save
          </Button>
        </form>
      );
    }
}

PatientForm.propTypes = {
  classes: PropTypes.object.isRequired,
  insertPatient: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({
  addPatientExpanded: state.Patient.addPatientExpanded,
});
const mapDispatchToProps = dispatch => bindActionCreators(patientActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(PatientForm));

