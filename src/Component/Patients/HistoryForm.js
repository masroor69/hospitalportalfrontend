import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import patientActions from './redux/action';

const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
    marginTop: '24px',
    marginRight: '-20px',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  history: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  historyChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  historyAdd: {
    margin: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 5,
  },
});

class HistoryForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      historyObject: [],
    };
  }
  handleChange = event => {
    this.setState({ historyObject: event.target.value });
  };
  insertHistory = () => {
    this.props.updateSelectedPatientHistory(
      this.props.selectedPatients,
      this.state.historyObject
    );
  };
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <FormControl className={classes.historyAdd}>
          <InputLabel htmlFor="select-multiple-chip">History</InputLabel>
          <Select
            multiple
            value={this.state.historyObject}
            onChange={this.handleChange}
            input={<Input id="select-multiple-chip" />}
            renderValue={selected => (
              <div
                key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                className={classes.chips}
              >
                {selected.map(value =>
                  <Chip
                    key={value.id}
                    label={value.title}
                    className={classes.chip}
                  />)}
              </div>
          )}
          >
            {this.props.diseases.map(name => (
              <MenuItem
                key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                value={name}
                style={{
                  fontWeight:
                    this.state.historyObject.indexOf(name) === -1
                      ? 100
                      : 900,
                }}
              >
                <Checkbox
                  key={new Date().getTime() + Math.random() + Math.round(Math.random() * 1000)}
                  checked={this.state.historyObject.indexOf(name) > -1}
                />
                {name.title}
              </MenuItem>
          ))}
          </Select>
        </FormControl>
        <Button
          style={styles.button}
          variant="raised"
          color="primary"
          onClick={this.insertHistory}
        >
        Update History
        </Button>
      </React.Fragment>
    );
  }
}
HistoryForm.propTypes = {
  classes: PropTypes.object.isRequired,
  diseases: PropTypes.array.isRequired,
  selectedPatients: PropTypes.object,
  updateSelectedPatientHistory: PropTypes.func.isRequired,
};

HistoryForm.defaultProps = {
  selectedPatients: {},
};

const mapStateToProps = (state) => ({
  diseases: state.Disease.diseases,
  selectedPatients: state.Patient.selectedPatients,
});

const mapDispatchToProps = dispatch => bindActionCreators(patientActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(HistoryForm));

