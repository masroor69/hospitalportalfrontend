/* eslint-disable no-unused-vars,import/prefer-default-export */
import * as types from './Type';

export default function reducer(state = {
  addPatientExpanded: false,
  expanded: false,
}, action) {
  switch (action.type) {
    case types.ADD_PATIENT_DIALOG_TOGGLE:
      return {
        ...state,
        addPatientExpanded: !state.addPatientExpanded
      };
    case types.PATIENTS_ARROW_BACK_TOGGLE:
      return {
        ...state,
        expanded: !state.expanded
      };
    case types.ADD_PATIENTS:
      return {
        ...state,
        patients: action.payload
      };
    case types.SELECTED_PATIENTS:
      return {
        ...state,
        selectedPatients: action.payload
      };
    case types.REMOVE_SELECTED_PATIENTS:
      return {
        ...state,
        selectedPatients: {}
      };
    case types.ADD_RELATED_HISTORY:
      return {
        ...state,
        relatedHistory: action.payload
      };
    case types.REMOVE_RELATED_HISTORY:
      return {
        ...state,
        relatedHistory: []
      };
    default:
      return state;
  }
}
