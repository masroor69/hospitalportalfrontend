/* eslint-disable no-unused-vars,import/prefer-default-export */
import axios from 'axios';
import * as types from './Type';
import { addError } from '../../Error/redux/action';
import { addingUpdate } from '../../redux/action';

export const addPatDialogToggle = () => async (dispatch) => {
  dispatch({
    type: types.ADD_PATIENT_DIALOG_TOGGLE,
  });
};
export const patArrowBackToggle = () => async (dispatch) => {
  dispatch({
    type: types.PATIENTS_ARROW_BACK_TOGGLE,
  });
};
export const addPatient = (patient) => async (dispatch) => {
  dispatch({
    type: types.ADD_PATIENTS,
    payload: patient
  });
};

export const fetchingPatients = () => async (dispatch) => {
  dispatch({
    type: types.FETCH_PATIENTS,
  });
  axios.get('https://nodeendpoint.herokuapp.com/patient/patients')
    .then((response) => {
      dispatch(addPatient(response.data.patients));
      dispatch({
        type: types.ADD_RELATED_HISTORY,
        payload: response.data.experiences
      });
    }).catch(error => {
    });
};

export const insertPatient = (data) => async (dispatch) => {
  dispatch({
    type: types.INSERT_PATIENTS,
  });
  axios.post('https://nodeendpoint.herokuapp.com/patient/create', data)
    .then((response) => {
      if (response.data.errors) {
        response.data.errors.map((err) =>
          dispatch(addError(err.msg)));
      }
      if (response.data.save) {
        dispatch(addPatDialogToggle());
        dispatch(addError('PATIENT INSERT SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const selectedPatient = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/patient/${id}`)
    .then((response) => {
      dispatch({
        type: types.ADD_RELATED_HISTORY,
        payload: response.data.historys
      });
      dispatch({
        type: types.SELECTED_PATIENTS,
        payload: response.data.patient
      });
    }).catch(error => {
      console.log(error.message);
    });
};


export const removeSelectedPatient = () => async (dispatch) => {
  dispatch({
    type: types.REMOVE_SELECTED_PATIENTS,
  });
  dispatch({
    type: types.REMOVE_RELATED_HISTORY,
  });
};

export const delteSelectedPatient = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/patient/delete/${id}`)
    .then((response) => {
      if (response.data.delete === 'done') {
        dispatch(patArrowBackToggle());
        dispatch(addError('PATIENT DELETE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const updateSelectedPatient = (id, data) => async (dispatch) => {
  axios.post(`https://nodeendpoint.herokuapp.com/patient/update/${id}`, data)
    .then((response) => {
      if (response.data.update === 'done') {
        dispatch(patArrowBackToggle());
        dispatch(addError('PATIENT UPDATE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const updateSelectedPatientHistory = (patient, data) => async (dispatch) => {
  if (data.length > 0) {
    const exIds = [];
    data.forEach((exp) => {
      exIds.push(exp._id);
    });
    axios.post(`https://nodeendpoint.herokuapp.com/patientHistory/create/${patient._id}`, { disasesList: exIds })
      .then((response) => {
        if (response.data.update === 'done') {
          dispatch(patArrowBackToggle());
          dispatch(addError('PATIENT EXPERIENCE INSERTED SUCCESS'));
          dispatch(addingUpdate());
        }
      }).catch(error => {
        console.log(error.message);
      });
  } else {
    dispatch(addError('Not Selected Experience'));
  }
};

export const removeSelectedPatientSelectedHistory = (expId, patientId) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/patientHistory/delete/${expId}/${patientId}`)
    .then((response) => {
      if (response.data.update === 'done') {
        dispatch(addError('SELECTED HISTORY REMOVED SUCCESS'));
      }
    }).catch(error => {
      dispatch(addError('SOME BAD THING HAPPENED REFRESH THE PAGE'));
      console.log(error.message);
    });
};

export default {
  addPatDialogToggle,
  fetchingPatients,
  addPatient,
  insertPatient,
  selectedPatient,
  removeSelectedPatient,
  patArrowBackToggle,
  delteSelectedPatient,
  updateSelectedPatient,
  updateSelectedPatientHistory,
  removeSelectedPatientSelectedHistory,
};
