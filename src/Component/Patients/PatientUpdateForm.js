/* eslint react/no-unused-state:0,camelcase:0,max-len:0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import patientActions from './redux/action';
import HistoryForm from './HistoryForm';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  history: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  historyChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  historyAdd: {
    margin: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 10,
  },
  switch: {
    marginLeft: 'auto'
  }
});

class DoctorUpdateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      smoking: '',
      title: '',
      name: '',
      age: '',
      id: '',
      relatedHistory: '',
    };
  }
  componentWillReceiveProps(nextProps) {
    const ex = [];
    if (Array.isArray(nextProps.relatedHistory)) {
      nextProps.relatedHistory.forEach((history) => {
        if (history.disease != null) {
          ex.push(history);
        }
      });
    }
    this.setState({
      smoking: nextProps.selectedPatients.smoking,
      title: nextProps.selectedPatients.title,
      name: nextProps.selectedPatients.name,
      age: nextProps.selectedPatients.age,
      id: nextProps.selectedPatients._id,
      relatedHistory: ex,
    });
  }
    selectHandleChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
    handleSwitchChange = name => event => {
      this.setState({ [name]: event.target.checked });
    };
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    handleDeleteHistory = (exp) => {
      this.props.removeSelectedPatientSelectedHistory(exp._id, this.props.selectedPatients._id);
      this.state.relatedHistory.splice(this.state.relatedHistory.indexOf(exp), 1);
      this.setState({ relatedHistory: this.state.relatedHistory });
    };
    submitForm = () => {
      this.props.updateSelectedPatient(this.state.id, {
        title: this.state.title,
        name: this.state.name,
        age: this.state.age,
        id: this.state.id,
      });
      this.setState({ isReady: true, debounce: 200 });
    };
  removePatient = () => {
    this.props.delteSelectedPatient(this.props.selectedPatients._id);
  };
  render() {
    const { classes } = this.props;
    return (
      <form>
        <FormGroup row>
          <div>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="title">Title</InputLabel>
              <Select
                value={this.state.title}
                onChange={this.selectHandleChange}
                input={<Input name="title" id="title" />}
                autoWidth
              >
                <MenuItem value="Mr">Mr</MenuItem>
                <MenuItem value="Mrs">Mrs</MenuItem>
                <MenuItem value="Miss">Miss</MenuItem>
              </Select>
              <FormHelperText>Mr,Mrs,Miss</FormHelperText>
            </FormControl>
          </div>
          <div className={classes.switch} >
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.smoking}
                  onChange={this.handleSwitchChange('smoking')}
                  value="Smoking"
                  color="primary"
                />
              }
              label="Smoking"
            />
          </div>
        </FormGroup>
        <div >
          <TextField
            id="FullName"
            label="Full Name"
            placeholder="John Doe"
            value={this.state.name}
            onChange={this.handleChange('name')}
            className={classes.textField}
            margin="normal"
          />
        </div>
        <div>
          <TextField
            id="age"
            label="age"
            value={this.state.age}
            onChange={this.handleChange('age')}
            type="number"
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            margin="normal"
          />
        </div>
        <div className={classes.history}>
          <Typography className={classes.headerText} component="p">
           Patient Disease:
          </Typography>
          { this.state.relatedHistory && this.state.relatedHistory.length > 0 && (
           this.state.relatedHistory.map((history) => (
           (history.disease != null) ?
             <Chip
               key={history._id}
               label={history.disease.title}
               onDelete={() => (this.handleDeleteHistory(history))}
               className={classes.historyChips}
             />
           :
             <span />
           ))
           )
           }
          { this.state.relatedHistory.length === 0 && (
          <Typography className={classes.emText} component="em">
           No History Was Register yet, add One .
          </Typography>
           )}
          <br />
          <HistoryForm />
        </div>
        <Button
          style={styles.button}
          variant="raised"
          color="inherit"
          onClick={this.submitForm}
        >
          UPDATE
        </Button>
        <Button
          style={styles.button}
          variant="raised"
          color="secondary"
          onClick={this.removePatient}
        >
          DELETE
        </Button>
      </form>
    );
  }
}

DoctorUpdateForm.propTypes = {
  classes: PropTypes.object.isRequired,
  delteSelectedPatient: PropTypes.func.isRequired,
  updateSelectedPatient: PropTypes.func.isRequired,
  removeSelectedPatientSelectedHistory: PropTypes.func.isRequired,
  selectedPatients: PropTypes.object,
  relatedHistory: PropTypes.array,
};

DoctorUpdateForm.defaultProps = {
  selectedPatients: {},
  relatedHistory: []
};

const mapStateToProps = (state) => ({
  addPatientExpanded: state.Patient.addPatientExpanded,
  selectedPatients: state.Patient.selectedPatients,
  relatedHistory: state.Patient.relatedHistory,
});
const mapDispatchToProps = dispatch => bindActionCreators(patientActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DoctorUpdateForm));

