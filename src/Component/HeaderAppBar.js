import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Avatar from '@material-ui/core/Avatar';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import axios from 'axios';

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 60,
    height: 60,
  },
  appBar: {
    backgroundColor: '#2c2e35'
  },
};

class HeaderAppBar extends Component {
  state = { source: null };
  componentDidMount() {
    axios
      .get(
        'https://nodeendpoint.herokuapp.com/images/logo.png',
        { responseType: 'arraybuffer' },
      )
      .then(response => {
        const base64 = btoa(new Uint8Array(response.data).reduce(
          (data, byte) => data + String.fromCharCode(byte),
          '',
        ));
        this.setState({ source: `data:;base64,${base64}` });
      });
  }
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <Avatar
              alt="Sugeries Clinic"
              src={this.state.source}
              className={classNames(classes.avatar, classes.bigAvatar)}
            />
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

HeaderAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(HeaderAppBar);
