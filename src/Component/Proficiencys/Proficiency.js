/* eslint no-mixed-operators:0 ,max-len:0 */
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ProficiencyForm from './ProficiencyForm';
import ProficiencyUpdateForm from './ProficiencyUpdateForm';
import actions from './redux/action';
import generalActions from '../redux/action';

const styles = ({
  root: {
    width: '100%',
    overflowX: 'hidden',
    position: 'relative',
  },
  table: {
    minWidth: '100%',
  },
  header: {
    textAlign: 'left',
    padding: '5px  0px  0px 5px',
  },
  headerText: {
    color: 'rgba(58, 50, 50, 0.5)',
    display: 'inline-block',
    paddingTop: '10px',
  },
  rightFloat: {
    float: 'Right',
  },
  action: {
    width: '10%',
  },
  collepsBox: {
    position: 'absolute',
    left: 0,
    top: '60px',
    minHeight: '82% !important',
    width: '100%',
    backgroundColor: 'white',
  },
  addCollepsBox: {
    top: '60px',
    minHeight: '82% !important',
    width: '100%',
    backgroundColor: 'white',
    textAlign: 'left',
  },
  xMark: {
    transform: 'rotate(45deg)'
  },
});

class Proficiency extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 3,
      page: 0,
    };
  }
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  handleExpandClick = (id) => {
    if (this.state.expandedId === id) {
      this.props.arrowBackToggle();
    } else {
      this.props.arrowBackToggle();
      this.props.selectedProficiency(id);
    }
  };
  backArrowClicked = () => {
    this.props.arrowBackToggle();
    this.props.removeSelectedProficiency();
  };
  addHandler = () => {
    this.props.addProfDialogToggle();
    this.props.addingUpdate();
  };
  render() {
    const { classes, data, addProfExpanded, } = this.props;
    if (addProfExpanded) {
      return (
        <Paper className={classes.root}>
          <div className={classes.header}>
            <Typography className={classes.headerText} variant="headline" component="h2">
              Add Proficiency
            </Typography>
            { this.state.expanded ?
              <IconButton aria-label="Add" className={classes.rightFloat} onClick={this.backArrowClicked}>
                <ArrowBackIcon />
              </IconButton>
            :
              <IconButton aria-label="Add" className={classnames(classes.rightFloat, classes.xMark)} onClick={this.addHandler}>
                <AddIcon />
              </IconButton>}
          </div>
          <Collapse className={classes.addCollepsBox} in={this.props.addProfExpanded} timeout="auto" unmountOnExit>
            <ProficiencyForm />
          </Collapse>
        </Paper>);
    }
    return (
      <Paper className={classes.root}>
        <div className={classes.header}>
          <Typography className={classes.headerText} variant="headline" component="h2">
            { this.props.expanded ? 'Proficiency Detail' : 'Proficiency List' }
          </Typography>
          { this.props.expanded ?
            <IconButton aria-label="Add" className={classes.rightFloat} onClick={this.backArrowClicked}>
              <ArrowBackIcon />
            </IconButton>
            :
            <IconButton aria-label="Add" className={classes.rightFloat} onClick={this.addHandler}>
              <AddIcon />
            </IconButton>}
        </div>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell className={classes.action} />
              <TableCell>Full Title</TableCell>
            </TableRow>
          </TableHead>
          {data && data.length > 0 && (
            <TableBody>
              {data.slice(
                          this.state.page * this.state.rowsPerPage,
                          this.state.page * this.state.rowsPerPage +
                          this.state.rowsPerPage
              ).map(each => (
                <TableRow key={each._id}>
                  <TableCell>
                    <IconButton
                      className={classnames(classes.expand, {
                        [classes.expandOpen]: this.props.expanded,
                      })}
                      onClick={() => this.handleExpandClick(each._id)}
                      aria-expanded={this.props.expanded}
                      aria-label="Show more"
                    >
                      <ExpandMoreIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {each.title}
                  </TableCell>
                </TableRow>
                  ))
              }
            </TableBody>
          )}
        </Table>
        <TablePagination
          component="div"
          count={data.length}
          rowsPerPage={this.state.rowsPerPage}
          page={this.state.page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
        <Collapse className={classes.collepsBox} in={this.props.expanded} timeout="auto" unmountOnExit>
          <ProficiencyUpdateForm />
        </Collapse>
      </Paper>);
  }
}

Proficiency.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired,
  addProfExpanded: PropTypes.bool.isRequired,
  addProfDialogToggle: PropTypes.func.isRequired,
  addingUpdate: PropTypes.func.isRequired,
  selectedProficiency: PropTypes.func.isRequired,
  removeSelectedProficiency: PropTypes.func.isRequired,
  arrowBackToggle: PropTypes.func.isRequired,
  expanded: PropTypes.bool.isRequired,
};


const mapStateToProps = (state) => ({
  addProfExpanded: state.Proficiency.addProfExpanded,
  expanded: state.Proficiency.expanded,
});
const mapDispatchToProps = dispatch => bindActionCreators(Object.assign({}, actions, generalActions), dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Proficiency));
