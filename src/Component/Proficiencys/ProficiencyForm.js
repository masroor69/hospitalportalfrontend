/* eslint react/no-unused-state: 0  camelcase: 0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import profActions from './redux/action';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  }
});

class ProficiencyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      title: '',
    };
  }
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    submitForm = () => {
      this.props.insertProficiency({
        title: this.state.title,
      });
      this.setState({ isReady: true, debounce: 200 });
    };
  removeProf = () => {
    this.props.delteSelectedProficiency(this.props.selectedProficiencys._id);
  };
  render() {
    const { classes } = this.props;
    return (
      <form>
        <div>
          <TextField
            id="Title"
            label="Title"
            placeholder="Physiology etc."
            onChange={this.handleChange('title')}
            className={classes.textField}
            margin="normal"
          />
        </div>
        <Button
          style={styles.button}
          variant="raised"
          color="primary"
          onClick={this.submitForm}
        >
            Save
        </Button>
      </form>
    );
  }
}

ProficiencyForm.propTypes = {
  classes: PropTypes.object.isRequired,
  insertProficiency: PropTypes.func.isRequired,
  delteSelectedProficiency: PropTypes.func.isRequired,
  selectedProficiencys: PropTypes.object,
};

ProficiencyForm.defaultProps = {
  selectedProficiencys: {}
};

const mapStateToProps = (state) => ({
  addExpanded: state.Doctors.addExpanded,
  selectedProficiencys: state.Proficiency.selectedProficiencys,
});
const mapDispatchToProps = dispatch => bindActionCreators(profActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ProficiencyForm));

