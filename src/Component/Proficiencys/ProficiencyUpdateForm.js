/* eslint react/no-unused-state:0,camelcase:0,max-len:0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import profActions from './redux/action';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  }
});

class ProficiencyUpdateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      title: '',
      id: ''
    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      title: nextProps.selectedProficiencys.title,
      id: nextProps.selectedProficiencys._id,
    });
  }
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    submitForm = () => {
      this.props.updateSelectedProficiency(this.state.id, {
        id: this.state.id,
        title: this.state.title,
      });
      this.setState({ isReady: true, debounce: 200 });
    };
  removeProf = () => {
    this.props.delteSelectedProficiency(this.props.selectedProficiencys._id);
  };
  render() {
    const { classes } = this.props;
    return (
      <form>
        <div>
          <TextField
            id="Title"
            label="Title"
            placeholder="Physiology etc."
            value={this.state.title}
            onChange={this.handleChange('title')}
            className={classes.textField}
            margin="normal"
          />
        </div>
        <Button
          style={styles.button}
          variant="raised"
          color="inherit"
          onClick={this.submitForm}
        >
          UPDATE
        </Button>
        <Button
          style={styles.button}
          variant="raised"
          color="secondary"
          onClick={this.removeProf}
        >
          DELETE
        </Button>
      </form>
    );
  }
}

ProficiencyUpdateForm.propTypes = {
  classes: PropTypes.object.isRequired,
  delteSelectedProficiency: PropTypes.func.isRequired,
  updateSelectedProficiency: PropTypes.func.isRequired,
  selectedProficiencys: PropTypes.object,
};

ProficiencyUpdateForm.defaultProps = {
  selectedProficiencys: {}
};

const mapStateToProps = (state) => ({
  addExpanded: state.Doctors.addExpanded,
  selectedProficiencys: state.Proficiency.selectedProficiencys,
});
const mapDispatchToProps = dispatch => bindActionCreators(profActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ProficiencyUpdateForm));

