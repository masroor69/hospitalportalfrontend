/* eslint-disable no-unused-vars,import/prefer-default-export */
import axios from 'axios';
import * as types from './Type';
import { addError } from '../../Error/redux/action';
import { addingUpdate } from '../../redux/action';

export const addProfDialogToggle = () => async (dispatch) => {
  dispatch({
    type: types.ADD_PROF_DIALOG_TOGGLE,
  });
};
export const arrowBackToggle = () => async (dispatch) => {
  dispatch({
    type: types.ARROW_BACK_TOGGLE,
  });
};
export const addProficiency = (proficiencys) => async (dispatch) => {
  dispatch({
    type: types.ADD_PROFICIENCYS,
    payload: proficiencys
  });
};

export const fetchingProficiency = () => async (dispatch) => {
  dispatch({
    type: types.FETCH_PROFICIENCYS,
  });
  axios.get('https://nodeendpoint.herokuapp.com/proficiency/proficiencys')
    .then((response) => {
      dispatch({
        type: types.ADD_PROFICIENCYS,
        payload: response.data.proficiencys
      });
    }).catch(error => {
    });
};

export const insertProficiency = (data) => async (dispatch) => {
  dispatch({
    type: types.INSERT_PROFICIENCY,
  });
  axios.post('https://nodeendpoint.herokuapp.com/proficiency/create', data)
    .then((response) => {
      if (response.data.errors) {
        response.data.errors.map((err) =>
          dispatch(addError(err.msg)));
      }
      if (response.data.save) {
        dispatch(addProfDialogToggle());
        dispatch(addError('PROFICIENCY INSERT SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const selectedProficiency = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/proficiency/${id}`)
    .then((response) => {
      dispatch({
        type: types.SELECTED_PROFICIENCYS,
        payload: response.data.proficiency
      });
    }).catch(error => {
      console.log(error.message);
    });
};

export const removeSelectedProficiency = () => async (dispatch) => {
  dispatch({
    type: types.REMOVE_SELECTED_PROFICIENCYS,
  });
};

export const delteSelectedProficiency = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/proficiency/delete/${id}`)
    .then((response) => {
      if (response.data.delete === 'done') {
        dispatch(arrowBackToggle());
        dispatch(addError('PROFICIENCY DELETE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const updateSelectedProficiency = (id, data) => async (dispatch) => {
  axios.post(`https://nodeendpoint.herokuapp.com/proficiency/update/${id}`, data)
    .then((response) => {
      if (response.data.update === 'done') {
        dispatch(arrowBackToggle());
        dispatch(addError('PROFICIENCY UPDATE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};
export default {
  addProfDialogToggle,
  fetchingProficiency,
  addProficiency,
  insertProficiency,
  selectedProficiency,
  removeSelectedProficiency,
  arrowBackToggle,
  delteSelectedProficiency,
  updateSelectedProficiency,
};
