import * as types from './Type';

export default function reducer(state = {
  addProfExpanded: false,
  expanded: false,
}, action) {
  switch (action.type) {
    case types.ADD_PROF_DIALOG_TOGGLE:
      return {
        ...state,
        addProfExpanded: !state.addProfExpanded
      };
    case types.ARROW_BACK_TOGGLE:
      return {
        ...state,
        expanded: !state.expanded
      };
    case types.ADD_PROFICIENCYS:
      return {
        ...state,
        proficiencys: action.payload
      };
    case types.SELECTED_PROFICIENCYS:
      return {
        ...state,
        selectedProficiencys: action.payload
      };
    case types.REMOVE_SELECTED_PROFICIENCYS:
      return {
        ...state,
        selectedProficiencys: {}
      };
    default:
      return state;
  }
}
