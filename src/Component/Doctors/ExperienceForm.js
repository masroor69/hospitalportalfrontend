import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import doctorActions from './redux/action';

const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
    marginTop: '24px',
    marginRight: '-20px',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  experience: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  experienceChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  experienceAdd: {
    margin: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 5,
  },
});

class ExperienceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      experienceObject: [],
    };
  }
  handleChange = event => {
    this.setState({ experienceObject: event.target.value });
  };
  insertExperience = () => {
    this.props.updateSelectedDoctorExperience(
      this.props.selectedDoctors,
      this.state.experienceObject
    );
  };
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <FormControl className={classes.experienceAdd}>
          <InputLabel htmlFor="select-multiple-chip">Experience</InputLabel>
          <Select
            multiple
            value={this.state.experienceObject}
            onChange={this.handleChange}
            input={<Input id="select-multiple-chip" />}
            renderValue={selected => (
              <div
                key={new Date().getTime() + Math.round(Math.random() * 1000)}
                className={classes.chips}
              >
                {selected.map(value =>
                  <Chip
                    key={value.id}
                    label={value.title}
                    className={classes.chip}
                  />)}
              </div>
          )}
          >
            {this.props.proficiencys.map(name => (
              <MenuItem
                key={new Date().getTime() + Math.round(Math.random() * 1000)}
                value={name}
                style={{
                  fontWeight:
                    this.state.experienceObject.indexOf(name) === -1
                      ? 100
                      : 900,
                }}
              >
                <Checkbox
                  key={new Date().getTime() + Math.round(Math.random() * 1000)}
                  checked={this.state.experienceObject.indexOf(name) > -1}
                />
                {name.title}
              </MenuItem>
          ))}
          </Select>
        </FormControl>
        <Button
          style={styles.button}
          variant="raised"
          color="primary"
          onClick={this.insertExperience}
        >
        Update Experience
        </Button>
      </React.Fragment>
    );
  }
}
ExperienceForm.propTypes = {
  classes: PropTypes.object.isRequired,
  proficiencys: PropTypes.array.isRequired,
  selectedDoctors: PropTypes.object,
  updateSelectedDoctorExperience: PropTypes.func.isRequired,
};

ExperienceForm.defaultProps = {
  selectedDoctors: {},
};

const mapStateToProps = (state) => ({
  proficiencys: state.Proficiency.proficiencys,
  selectedDoctors: state.Doctors.selectedDoctors,
});

const mapDispatchToProps = dispatch => bindActionCreators(doctorActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ExperienceForm));

