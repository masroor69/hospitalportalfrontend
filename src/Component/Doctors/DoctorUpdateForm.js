/* eslint react/no-unused-state:0,camelcase:0,max-len:0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import doctorActions from './redux/action';
import ExperienceForm from './ExperienceForm';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  experience: {
    marginLeft: '10px',
    marginRight: '30px',
  },
  experienceChips: {
    margin: '5px',
  },
  emText: {
    color: '#989898',
  },
  experienceAdd: {
    margin: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 10,
  },
});

class DoctorUpdateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      title: '',
      name: '',
      age: '',
      id: '',
      relatedExperience: '',
    };
  }
  componentWillReceiveProps(nextProps) {
    const ex = [];
    if (Array.isArray(nextProps.relatedExperience)) {
      nextProps.relatedExperience.forEach((experience) => {
        if (experience.proficiency != null) {
          ex.push(experience);
        }
      });
    }
    this.setState({
      title: nextProps.selectedDoctors.title,
      name: nextProps.selectedDoctors.name,
      age: nextProps.selectedDoctors.age,
      id: nextProps.selectedDoctors._id,
      relatedExperience: ex,
    });
  }
    selectHandleChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    handleDeleteExperience = (exp) => {
      this.props.removeSelectedDoctorSelectedExperience(exp._id, this.props.selectedDoctors._id);
      this.state.relatedExperience.splice(this.state.relatedExperience.indexOf(exp), 1);
      this.setState({ relatedExperience: this.state.relatedExperience });
    };
    submitForm = () => {
      this.props.updateSelectedDoctor(this.state.id, {
        title: this.state.title,
        name: this.state.name,
        age: this.state.age,
        id: this.state.id,
      });
      this.setState({ isReady: true, debounce: 200 });
    };
  removeDoctor = () => {
    this.props.delteSelectedDoctor(this.props.selectedDoctors._id);
  };
  render() {
    const { classes } = this.props;
    return (
      <form>
        <div>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="title">Title</InputLabel>
            <Select
              value={this.state.title}
              onChange={this.selectHandleChange}
              input={<Input name="title" id="title" />}
              autoWidth
            >
              <MenuItem value="Mr">Mr</MenuItem>
              <MenuItem value="Mrs">Mrs</MenuItem>
              <MenuItem value="Miss">Miss</MenuItem>
            </Select>
            <FormHelperText>Mr,Mrs,Miss</FormHelperText>
          </FormControl>
        </div>
        <div>
          <TextField
            id="FullName"
            label="Full Name"
            placeholder="John Doe"
            value={this.state.name}
            onChange={this.handleChange('name')}
            className={classes.textField}
            margin="normal"
          />
        </div>
        <div>
          <TextField
            id="age"
            label="age"
            value={this.state.age}
            onChange={this.handleChange('age')}
            type="number"
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            margin="normal"
          />
        </div>
        <div className={classes.experience}>
          <Typography className={classes.headerText} component="p">
            Doctor Experience:
          </Typography>
          { this.state.relatedExperience && this.state.relatedExperience.length > 0 && (
            this.state.relatedExperience.map((experience) => (
               (experience.proficiency != null) ?
                 <Chip
                   key={experience._id}
                   label={experience.proficiency.title}
                   onDelete={() => (this.handleDeleteExperience(experience))}
                   className={classes.experienceChips}
                 />
               :
                 <span />
          ))
            )
          }
          { this.state.relatedExperience.length === 0 && (
          <Typography className={classes.emText} component="em">
            No Experience Was Register yet, add One .
          </Typography>
          )}
          <br />
          <ExperienceForm />
        </div>
        <Button
          style={styles.button}
          variant="raised"
          color="inherit"
          onClick={this.submitForm}
        >
          UPDATE
        </Button>
        <Button
          style={styles.button}
          variant="raised"
          color="secondary"
          onClick={this.removeDoctor}
        >
          DELETE
        </Button>
      </form>
    );
  }
}

DoctorUpdateForm.propTypes = {
  classes: PropTypes.object.isRequired,
  delteSelectedDoctor: PropTypes.func.isRequired,
  updateSelectedDoctor: PropTypes.func.isRequired,
  removeSelectedDoctorSelectedExperience: PropTypes.func.isRequired,
  selectedDoctors: PropTypes.object,
  relatedExperience: PropTypes.array,
};

DoctorUpdateForm.defaultProps = {
  selectedDoctors: {},
  relatedExperience: []
};

const mapStateToProps = (state) => ({
  addDoctorExpanded: state.Doctors.addDoctorExpanded,
  selectedDoctors: state.Doctors.selectedDoctors,
  relatedExperience: state.Doctors.relatedExperience,
});
const mapDispatchToProps = dispatch => bindActionCreators(doctorActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DoctorUpdateForm));

