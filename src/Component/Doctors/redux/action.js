/* eslint-disable no-unused-vars,import/prefer-default-export */
import axios from 'axios';
import * as types from './Type';
import { addError } from '../../Error/redux/action';
import { addingUpdate } from '../../redux/action';

export const addDocDialogToggle = () => async (dispatch) => {
  dispatch({
    type: types.ADD_DOCTOR_DIALOG_TOGGLE,
  });
};
export const docArrowBackToggle = () => async (dispatch) => {
  dispatch({
    type: types.DOCTORS_ARROW_BACK_TOGGLE,
  });
};
export const addDoctor = (doctor) => async (dispatch) => {
  dispatch({
    type: types.ADD_DOCTORS,
    payload: doctor
  });
};

export const fetchingDoctors = () => async (dispatch) => {
  dispatch({
    type: types.FETCH_DOCTORS,
  });
  axios.get('https://nodeendpoint.herokuapp.com/doctor/doctors')
    .then((response) => {
      dispatch(addDoctor(response.data.doctors));
      dispatch({
        type: types.ADD_RELATED_EXPERIENCE,
        payload: response.data.experiences
      });
    }).catch(error => {
      console.log(error.message);
    });
};

export const insertDoctor = (data) => async (dispatch) => {
  dispatch({
    type: types.INSERT_DOCTORS,
  });
  axios.post('https://nodeendpoint.herokuapp.com/doctor/create', data)
    .then((response) => {
      if (response.data.errors) {
        response.data.errors.map((err) =>
          dispatch(addError(err.msg)));
      }
      if (response.data.save) {
        dispatch(addDocDialogToggle());
        dispatch(addError('DOCOTOR INSERT SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const selectedDoctor = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/doctor/${id}`)
    .then((response) => {
      dispatch({
        type: types.ADD_RELATED_EXPERIENCE,
        payload: response.data.experiences
      });
      dispatch({
        type: types.SELECTED_DOCTORS,
        payload: response.data.doctor
      });
    }).catch(error => {
      console.log(error.message);
    });
};


export const removeSelectedDoctor = () => async (dispatch) => {
  dispatch({
    type: types.REMOVE_SELECTED_DOCTORS,
  });
  dispatch({
    type: types.REMOVE_RELATED_EXPERIENCE,
  });
};

export const delteSelectedDoctor = (id) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/doctor/delete/${id}`)
    .then((response) => {
      if (response.data.delete === 'done') {
        dispatch(docArrowBackToggle());
        dispatch(addError('DOCTOR DELETE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const updateSelectedDoctor = (id, data) => async (dispatch) => {
  axios.post(`https://nodeendpoint.herokuapp.com/doctor/update/${id}`, data)
    .then((response) => {
      if (response.data.update === 'done') {
        dispatch(docArrowBackToggle());
        dispatch(addError('DOCTOR UPDATE SUCCESS'));
        dispatch(addingUpdate());
      }
    }).catch(error => {
      console.log(error.message);
    });
};

export const updateSelectedDoctorExperience = (doctor, data) => async (dispatch) => {
  if (data.length > 0) {
    const exIds = [];
    data.forEach((exp) => {
      exIds.push(exp._id);
    });
    axios.post(`https://nodeendpoint.herokuapp.com/doctorExperience/create/${doctor._id}`, { proficiencyList: exIds })
      .then((response) => {
        if (response.data.update === 'done') {
          dispatch(docArrowBackToggle());
          dispatch(addError('DOCTOR EXPERIENCE INSERTED SUCCESS'));
          dispatch(addingUpdate());
        }
      }).catch(error => {
        console.log(error.message);
      });
  } else {
    dispatch(addError('Not Selected Experience'));
  }
};

export const removeSelectedDoctorSelectedExperience = (expId, doctorId) => async (dispatch) => {
  axios.get(`https://nodeendpoint.herokuapp.com/doctorExperience/delete/${expId}/${doctorId}`)
    .then((response) => {
      if (response.data.update === 'done') {
        dispatch(addError('SELECTED EXPERIENCE REMOVED SUCCESS'));
      }
    }).catch(error => {
      dispatch(addError('SOME BAD THING HAPPENED REFRESH THE PAGE'));
      console.log(error.message);
    });
};

export default {
  addDocDialogToggle,
  fetchingDoctors,
  addDoctor,
  insertDoctor,
  selectedDoctor,
  removeSelectedDoctor,
  docArrowBackToggle,
  delteSelectedDoctor,
  updateSelectedDoctor,
  updateSelectedDoctorExperience,
  removeSelectedDoctorSelectedExperience,
};
