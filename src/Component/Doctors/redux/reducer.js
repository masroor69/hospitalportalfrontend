/* eslint-disable no-unused-vars,import/prefer-default-export, */
import * as types from './Type';

export default function reducer(state = {
  addDoctorExpanded: false,
  expanded: false,
}, action) {
  switch (action.type) {
    case types.ADD_DOCTOR_DIALOG_TOGGLE:
      return {
        ...state,
        addDoctorExpanded: !state.addDoctorExpanded
      };
    case types.DOCTORS_ARROW_BACK_TOGGLE:
      return {
        ...state,
        expanded: !state.expanded
      };
    case types.ADD_DOCTORS:
      return {
        ...state,
        doctors: action.payload
      };
    case types.SELECTED_DOCTORS:
      return {
        ...state,
        selectedDoctors: action.payload
      };
    case types.REMOVE_SELECTED_DOCTORS:
      return {
        ...state,
        selectedDoctors: {}
      };
    case types.ADD_RELATED_EXPERIENCE:
      return {
        ...state,
        relatedExperience: action.payload
      };
    case types.REMOVE_RELATED_EXPERIENCE:
      return {
        ...state,
        relatedExperience: []
      };
    default:
      return state;
  }
}
