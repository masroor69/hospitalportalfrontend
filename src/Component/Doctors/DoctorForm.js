/* eslint react/no-unused-state: 0  camelcase: 0 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import doctorActions from './redux/action';


const styles = ({
  loader: {
    margin: '0 auto',
    display: 'block'
  },
  button: {
    margin: '8px',
    float: 'right',
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: '10px',
    marginRight: '30px',
    width: '80%',
  },
  formControl: {
    marginLeft: '10px',
    marginRight: '30px',
  }
});

class DoctorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: false,
      title: '',
      name: '',
      age: '',
    };
  }
  selectHandleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };
    submitForm = () => {
      this.props.insertDoctor({
        title: this.state.title,
        name: this.state.name,
        age: this.state.age,
      });
      this.setState({ isReady: true, debounce: 200 });
    };
    render() {
      const { classes } = this.props;
      return (
        <form>
          <div>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="title">Title</InputLabel>
              <Select
                value={this.state.title}
                onChange={this.selectHandleChange}
                input={<Input name="title" id="title" />}
                autoWidth
              >
                <MenuItem value="Mr">Mr</MenuItem>
                <MenuItem value="Mrs">Mrs</MenuItem>
                <MenuItem value="Miss">Miss</MenuItem>
              </Select>
              <FormHelperText>Mr,Mrs,Miss</FormHelperText>
            </FormControl>
          </div>
          <div>
            <TextField
              id="FullName"
              label="Full Name"
              placeholder="John Doe"
              onChange={this.handleChange('name')}
              className={classes.textField}
              margin="normal"
            />
          </div>
          <div>
            <TextField
              id="age"
              label="age"
              value={this.state.age}
              onChange={this.handleChange('age')}
              type="number"
              className={classes.textField}
              InputLabelProps={{
              shrink: true,
            }}
              margin="normal"
            />
          </div>
          <Button
            style={styles.button}
            variant="raised"
            color="primary"
            onClick={this.submitForm}
          >
          Save
          </Button>
        </form>
      );
    }
}

DoctorForm.propTypes = {
  classes: PropTypes.object.isRequired,
  insertDoctor: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({
  addDoctorExpanded: state.Doctors.addDoctorExpanded,
});
const mapDispatchToProps = dispatch => bindActionCreators(doctorActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DoctorForm));

