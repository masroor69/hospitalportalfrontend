/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import HeaderAppBar from './HeaderAppBar';
import MainNavigation from './MainNavigation';
import Errors from './Error/Errors';
import generalActions from './redux/action';

class App extends Component {
  render() {
    const { error } = this.props;
    console.log(this.props.update);
    return (
      <Fragment>
        {error.map((message, i) =>
          <div key={new Date().getTime() + Math.round(Math.random() * 1000)} >
            <Errors message={message} index={i} />
          </div>)}
        <HeaderAppBar />
        <MainNavigation />
      </Fragment>
    );
  }
}

App.propTypes = {
  error: PropTypes.array,
  update: PropTypes.number.isRequired,
};
App.defaultProps = {
  error: []
};
const mapStateToProps = (state) => ({
  error: state.Errors,
  update: state.General.update,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(Object.assign({}, generalActions), dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(App);
