/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import App from './App';
import doctorActions from './Doctors/redux/action';
import profActions from './Proficiencys/redux/action';
import diseasesActions from './Diseases/redux/action';
import patientsActions from './Patients/redux/action';
import generalActions from './redux/action';
import surgurieActions from './Surgerie/redux/action';

class AppContiner extends Component {
  componentWillUpdate() {
    this.props.fetchingDoctors();
    this.props.fetchingProficiency();
    this.props.fetchingDiseases();
    this.props.fetchingPatients();
    this.props.fetchingSurgerie();
  }
  render() {
    console.log(this.props.update);
    return (
      <Fragment>
        <App />
      </Fragment>
    );
  }
}

AppContiner.propTypes = {
  update: PropTypes.number.isRequired,
  fetchingDoctors: PropTypes.func.isRequired,
  fetchingProficiency: PropTypes.func.isRequired,
  fetchingDiseases: PropTypes.func.isRequired,
  fetchingPatients: PropTypes.func.isRequired,
  fetchingSurgerie: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  error: state.Errors,
  update: state.General.update,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(Object.assign({}, doctorActions, generalActions, profActions, diseasesActions, patientsActions, surgurieActions), dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(AppContiner);
