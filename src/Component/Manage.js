/* eslint-disable max-len */
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Doctor from './Doctors/Doctor';
import Patient from './Patients/Patient';
import Proficiency from './Proficiencys/Proficiency';
import Disease from './Diseases/Disease';
import generalActions from './redux/action';
import doctorActions from './Doctors/redux/action';
import profiActions from './Proficiencys/redux/action';
import diasesAction from './Diseases/redux/action';

const styles = ({
  root: {
    flexGrow: 1,
  },
});

class Manage extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={8}>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <Doctor data={this.props.doctors} />
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <Patient data={this.props.patients} />
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <Proficiency data={this.props.proficiencys} />
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <Disease data={this.props.diseases} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}


Manage.propTypes = {
  classes: PropTypes.object.isRequired,
  doctors: PropTypes.array,
  proficiencys: PropTypes.array,
  diseases: PropTypes.array,
  patients: PropTypes.array,
};
Manage.defaultProps = {
  doctors: [],
  proficiencys: [],
  diseases: [],
  patients: [],
};

const mapStateToProps = (state) => ({
  update: state.General.update,
  doctors: state.Doctors.doctors,
  proficiencys: state.Proficiency.proficiencys,
  diseases: state.Disease.diseases,
  patients: state.Patient.patients,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(Object.assign({}, doctorActions, profiActions, generalActions, diasesAction), dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Manage));
